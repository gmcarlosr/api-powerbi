const pool = require("../dbconfig.js");

const getGu_usuarios = async (req, res) => {
    try{
        const conn = await pool.getConnection();
        const query = "SELECT * FROM gu_usuario";
        const gu_usuario = await conn.query(query);
        conn.end();
    
        if(gu_usuario.length === 0){
          res.json([]);
        }
        else{
          res.status(200).json(gu_usuario);
        }
      }
      catch(e){
        console.log(e);
        res.status(500).json({message:"Error"});
      }
}

const getGu_usuariosPagination = async (req, res) => {
    try{
        const conn = await pool.getConnection();
        const { page, size } = req.query;
        const offset = (page - 1) * size;
        const query = "SELECT * FROM gu_usuario LIMIT ?, ?";
        const gu_usuario = await conn.query(query, [offset, size]);
        conn.end();
    
        if(gu_usuario.length === 0){
          res.json([]);
        }
        else{
          res.status(200).json(gu_usuario);
        }
      }
      catch(e){
        console.log(e);
        res.status(500).json({message:"Error"});
      }
}

const getGu_usuariosById = async (req, res) => {
    try{
        const conn = await pool.getConnection();
        const { id } = req.params;
        const query = "SELECT * FROM gu_usuario WHERE id_usuario = ? ";
        const gu_usuario = await conn.query(query, [id]);
        conn.end();
    
        if(gu_usuario.length === 0){
          res.status(404).json({message:"gu_usuario no encontrado"});
        }
        else{
          res.status(200).json(gu_usuario[0]);
        }
      }
      catch(e){
        console.log(e);
        res.status(500).json({message:"Error"});
      }
}

const deleteGu_usuariosById = async (req, res) => {
    try{
        const conn = await pool.getConnection();
        const { id } = req.params;
        const query = "DELETE FROM gu_usuario WHERE id_usuario = ? ";
        const gu_usuario = await conn.query(query, [id]);
        conn.end();
    
        if(gu_usuario.length === 0){
          res.status(404).json({message:"gu_usuario no encontrado"});
        }
        else{
          res.status(200).json({message:"gu_usuario eliminado"});
        }
      }
      catch(e){
        console.log(e);
        res.status(500).json({message:"Error"});
      }
}

const createGu_usuarios = async (req, res) => {
    try{
      const conn = await pool.getConnection();
      const {
          id_rol,
          usuario,
          clave,
          nombres,
          apellidos,
          avatar,
          refresh_token,
          access_token,
          estado
      }   = req.body;
  
  
      if(id_rol == undefined || id_rol == null || id_rol === ""){
          res.status(400).json({message:"Error: El id del rol no puede estar vacío"});
        }
      if(usuario == undefined || usuario == null || usuario === ""){
          res.status(400).json({message:"Error: El usuario no puede estar vacío"});
        }
      if(clave == undefined || clave == null || clave === ""){
          res.status(400).json({message:"Error: La clave no puede estar vacía"});
        }
      if(nombres == undefined || nombres == null || nombres === ""){
        res.status(400).json({message:"Error: Los nombres no pueden estar vacíos"});
      }
      if(apellidos == undefined || apellidos == null || apellidos === ""){
          res.status(400).json({message:"Error: Los apellidos no pueden estar vacíos"});
        }
      if(avatar == undefined || avatar == null || avatar === ""){
          res.status(400).json({message:"Error: El avatar no puede estar vacío"});
        }
      if(refresh_token == undefined || refresh_token == null || refresh_token === ""){
          res.status(400).json({message:"Error: El refrsh token no puede estar vacío"});
        }
      if(access_token == undefined || access_token == null || access_token === ""){
          res.status(400).json({message:"Error: El access token no puede estar vacío"});
        }
      if(estado == undefined || estado == null || estado === ""){
        res.status(400).json({message:"Error: El estado no puede estar vacío"});
      }

    
      const query = "INSERT INTO gu_usuario (id_rol, usuario, clave, nombres, apellidos, avatar, refresh_token, access_token, estado) VALUES (?,?,?,?,?,?,?,?,?) ";
      try{
        await conn.query(query, [
          id_rol,
          usuario,
          clave,
          nombres,
          apellidos,
          avatar,
          refresh_token,
          access_token,
          estado
        ]);
        conn.end();
        res.status(201).json({message:"El usuario ha sido agregado exitosamente"});
      }
      catch(e){
        console.log(e);
        res.status(404).json({message:"Error"});
      }
      }
      catch(e){
        console.log(e);
        res.status(500).json({message:"Error"});
      }
}

const updateGu_usuarios = async (req, res) => {
    try{
      const conn = await pool.getConnection();
      const {
        id_usuario,
        id_rol,
        usuario,
        clave,
        nombres,
        apellidos,
        avatar,
        refresh_token,
        access_token,
        estado
    }   = req.body;
      let query = "UPDATE gu_usuario SET ";
      const values = [];

      if (id_rol) {
        query += "id_rol = ?, ";
        values.push(id_rol);
      }
      if (usuario) {
        query += "usuario = ?, ";
        values.push(usuario);
      }
      if (clave) {
        query += "clave = ?, ";
        values.push(clave);
      }
      if (nombres) {
        query += "nombres = ?, ";
        values.push(nombres);
      }
      if (apellidos) {
        query += "apellidos = ?, ";
        values.push(apellidos);
      }
      if (avatar) {
        query += "avatar = ?, ";
        values.push(avatar);
      }
      if (refresh_token) {
        query += "refresh_token = ?, ";
        values.push(refresh_token);
      }
      if (access_token) {
        query += "access_token = ?, ";
        values.push(access_token);
      }
      if (estado) {
        query += "estado = ?, ";
        values.push(estado);
      }

      query = query.slice(0, -2) + " WHERE id_usuario = ?";
      values.push(id_usuario);
    
      try {
        await conn.query(query, values);
        conn.end();
        res.status(200).json({ message: "Usuario actualizado exitosamente" });
      } catch (e) {
        console.log(e);
        res.status(500).json({ message:" Error "});
      }
      }
      catch(e){
        console.log(e);
        res.status(500).json({message:"Error"});
      }
}

module.exports = {
    getGu_usuarios: getGu_usuarios,
    getGu_usuariosPagination: getGu_usuariosPagination,
    getGu_usuariosById: getGu_usuariosById,
    deleteGu_usuariosById: deleteGu_usuariosById,
    createGu_usuarios: createGu_usuarios,
    updateGu_usuarios: updateGu_usuarios
}
