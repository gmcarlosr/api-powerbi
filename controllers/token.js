const pool = require("../dbconfig.js");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
const SECRET_KEY = process.env.SECRET_KEY;

const token = async (req, res) => {
  const conn = await pool.getConnection();
  const { usuario, clave } = req.query;
  if (usuario === "") {
    res.json({ message: `Error: El usuario no puede estar vacío` });
    return;
  }
  if (clave === "") {
    res.json({ message: `Error: La clave no puede estar vacío` });
    return;
  }
  const query = `SELECT id_usuario,id_rol,usuario FROM gu_usuario WHERE usuario = ? AND clave = ?`;
  try {
    const rows = await conn.query(query, [usuario, clave]);
    conn.end();
    if (rows.length > 0) {
      const userData = rows[0];
      const accessToken = jwt.sign({ usuario, userData }, SECRET_KEY, {
        expiresIn: "1h",
      });
      const refreshToken = await bcrypt.hash(usuario, 10);
      res.json({ accessToken, refreshToken, userData });
    } else {
      res.status(400).json({ error: "Invalid Credentials" });
    }
  } catch (e) {
    console.log(e);
    res.json({ message: `Error` });
  }
};

const refresh = async (req, res) => {
  try {
    const { refreshToken, usuario } = req.body;
    if (bcrypt.compare(usuario, refreshToken)) {
      const accessToken = jwt.sign({ usuario }, SECRET_KEY, {
        expiresIn: "4h",
      });
      res.json({ accessToken });
    } else {
      res.sendStatus(401);
    }
  } catch (e) {
    console.log(e);
    res.status(401).json({ error: "Invalid Credentials" });
  }
};

module.exports = {
  token: token,
  refresh: refresh,
};
