require("dotenv").config();
const mariadb = require("mariadb");

  const config = {
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    user: process.env.DB_USER,
    password:  process.env.DB_PASS,
    database: process.env.DB_DATABASE,
    connectionLimit: 2
  };

const pool = mariadb.createPool(config);
module.exports = pool;
  