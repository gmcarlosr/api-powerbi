const bcrypt = require('bcrypt');
const saltRounds = 10;
const plainPassword = 'admin';

bcrypt.hash(plainPassword, saltRounds, (err, hash) => {
  if (err) {
    console.error(err);
    return;
  }
  console.log('Contraseña encriptada:', hash);
});