const express = require("express");
var cors = require('cors')
const app = express();
const routerApi = require('./routes');
require("dotenv").config();
var config = require("./dbconfig.js");
app.use(cors());
app.use(express.json());

routerApi(app);

app.listen(4000);
console.log("Server listen 4000");

