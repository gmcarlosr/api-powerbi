const jwt = require('jsonwebtoken');
const SECRET_KEY = process.env.SECRET_KEY;

function destroyToken(req, res, next) {
    const token = req.headers['authorization'];
    let bearerToken = '';
    if (token) {
      const bearer = token.split(' ');
      bearerToken = bearer[1];
    }
    if (!bearerToken) {
      return res.status(401).json({
        auth: false,
        message: "No token provided"
      });
    }
  
    jwt.sign({}, SECRET_KEY, { expiresIn: 0 }, (err, decoded) => {
      if (err) {
        return res.status(500).json({
          auth: false,
          message: "Failed to destroy token"
        });
      }
      next();
    });
  }

   
module.exports = destroyToken;
