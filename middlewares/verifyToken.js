const jwt = require('jsonwebtoken');
const SECRET_KEY = process.env.SECRET_KEY;

function verifyToken(req, res, next) {
    const token = req.headers['authorization'];
    let bearerToken = '';
    if (token) {
      const bearer = token.split(' ');
      bearerToken = bearer[1];
    }
    if (!bearerToken) {
      return res.status(401).json({
        auth: false,
        message: "No token provided"
      });
    }
  
    jwt.verify(bearerToken, SECRET_KEY, (err, decoded) => {
      if (err) {
        return res.status(500).json({
          auth: false,
          message: "Failed to authenticate token"
        });
      }
      req.userId    =   decoded.usuario;
      req.userData  =   decoded.userData;
      next();
    });
  
  }

 

module.exports = verifyToken;
