const { Sequelize, DataTypes } = require('sequelize');
const dbConfig = require("../dbconfig.js");

const sequelize = new Sequelize(dbConfig.development);

const Usuario = sequelize.define('Usuario', {
  IdUsuario: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true,
  },
  IdRol: {
    type: DataTypes.INTEGER,
    allowNull: false,
  },
  Usuario: {
    type: DataTypes.STRING,
    allowNull: false,
  },
  Clave: {
    type: DataTypes.STRING,
    allowNull: false,
    set(value) {
      const bcrypt = require('bcrypt');
      const saltRounds = 10;
      const hashedPassword = bcrypt.hashSync(value, saltRounds);
      this.setDataValue('clave', hashedPassword);
    },
  },
}, {
  timestamps: false,
  modelName: 'Usuario',
  tableName: 'Usuario'
});

module.exports = Usuario;