const express = require('express');
const router = express.Router();
const pool = require("../dbconfig.js"); 

const verifyToken = require('../middlewares/verifyToken');
const guUsuariosController = require("../controllers/gu_usuarios.js");
//const { validateCreate, validateUpdate } = require('../validators/colaborador');

  
  //Mostrar todos los gu_usuarios
  router.get("/", verifyToken, async (req, res) => {
      guUsuariosController.getGu_usuarios(req, res);
  }); 

  //Mostrar los gu_usuarios con paginacion
  router.get("/pagination", verifyToken, async (req, res) => {
    guUsuariosController.getGu_usuariosPagination(req, res); 
  });
  
  //Mostrar un solo gu_usuario
  router.get("/:id", verifyToken, async(req, res) =>{
    guUsuariosController.getGu_usuariosById(req, res);
  });
  
  //Eliminar un gu_usuario
  router.delete("/:id", verifyToken, async(req, res) => {
     guUsuariosController.deleteGu_usuarios(req, res);
  });
  
  //Agregar un gu_usuario
  router.post("/", verifyToken, async(req, res) => {
    guUsuariosController.createGu_usuarios(req, res);
  });

  //Actualizar un gu_usuario
  router.put("/:id", verifyToken, async(req, res) => {
    guUsuariosController.updateGu_usuarios(req, res);
  });

  module.exports = router;