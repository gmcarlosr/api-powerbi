const express = require('express');
const tokensRouter = require('./token.router');
const gu_usuariosRouter = require('./gu_usuarios.router');

function routerApi(app){
    const router = express.Router();
    app.use('/api/v1',router);
    router.use('/token', tokensRouter);
    router.use('/gu_usuarios', gu_usuariosRouter);
}

module.exports = routerApi;