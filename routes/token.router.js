const express = require('express');
const router = express.Router();
const tokenController = require('../controllers/token');

  //Obtener token
  router.get('/', async (req, res) => {
    tokenController.token(req, res);
  });
  
  //Refrescar token
  router.post('/refresh', (req, res) => {
    tokenController.refresh(req, res);
  });
  

  
  module.exports = router;